package ru.t1.ktitov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.dto.ISessionDtoService;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    public SessionDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}
